package model.vo;

import model.data_structures.DoublyLinkedList;

public class VOSector implements Comparable<VOSector>{
	private int id;
	private double maxLongitud;
	private double minLongitud;
	private double maxLatitud;
	private double minLatitud;

	private DoublyLinkedList<VOBikeRoute> cicloRutasPorSector;
	private DoublyLinkedList<VOStation> estacionesPorSector;


	public VOSector(int id, double maxLongitud, double minLongitud, double maxLatitud, double minLatitud) {
		this.id = id;
		this.maxLongitud = maxLongitud;
		this.minLongitud = minLongitud;
		this.maxLatitud = maxLatitud;
		this.minLatitud = minLatitud;
		this.cicloRutasPorSector = new DoublyLinkedList<VOBikeRoute>();
		this.setEstacionesPorSector(new DoublyLinkedList<VOStation>());
	}



	public VOSector() {

	}




	public void setId(DoublyLinkedList<VOBikeRoute> cicloRutasPorSector) {
		this.cicloRutasPorSector = cicloRutasPorSector;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getMaxLongitud() {
		return maxLongitud;
	}
	public void setMaxLongitud(double maxLongitud) {
		this.maxLongitud = maxLongitud;
	}
	public double getMinLongitud() {
		return minLongitud;
	}
	public void setMinLongitud(double minLongitud) {
		this.minLongitud = minLongitud;
	}
	public double getMaxLatitud() {
		return maxLatitud;
	}
	public void setMaxLatitud(double maxLatitud) {
		this.maxLatitud = maxLatitud;
	}
	public double getMinLatitud() {
		return minLatitud;
	}
	public void setMinLatitud(double minLatitud) {
		this.minLatitud = minLatitud;
	}
	public void setCicloRutasPorSector(DoublyLinkedList<VOBikeRoute> cicloRutasPorSector) {
		this.cicloRutasPorSector = cicloRutasPorSector;
	}
	public DoublyLinkedList<VOBikeRoute> getCicloRutasPorSector() {
		return cicloRutasPorSector;
	}

	public DoublyLinkedList<VOStation> getEstacionesPorSector() {
		return estacionesPorSector;
	}

	public void setEstacionesPorSector(DoublyLinkedList<VOStation> estacionesPorSector) {
		this.estacionesPorSector = estacionesPorSector;
	}

	@Override
	public int compareTo(VOSector o) {
		if(id>o.getId()) {
			return 1;
		}else if(id<o.getId()) {
			return -1;
		}else {
			return 0;
		}
	}




}
