package model.vo;

/**
 * Representation of a bike object
 */
public class VOBike implements Comparable<VOBike>{

	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------

	private int id;

	private int totalViajes; 

	private double totalDistancia;

	private int duracionViajes;



	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------

	public VOBike(int id) {
		super();
		this.id = id;
		this.totalViajes = 0;
		this.totalDistancia = 0;
		this.duracionViajes = 0;
	}

	public VOBike() {
		this.id = 0;
		this.totalViajes = 0;
		this.totalDistancia = 0;
		this.duracionViajes = 0;

	}

	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------
	public int darId() {
		return id;
	}

	public int getTotalViajes() {
		return totalViajes;
	}

	public void addOneTotalViajes() {
		this.totalViajes ++;
	}

	public double getTotalDistancia() {
		return totalDistancia;
	}

	public int getDuracionTotalViajes() {
		return duracionViajes;
	}

	public void sumarADuracionViajes(int duracion) {
		this.duracionViajes += duracion;
	}
	public void addDistanceToTotalDistancia(double totalDistancia) {
		this.totalDistancia += totalDistancia;
	}


	@Override
	public int compareTo(VOBike o) {
		if(totalViajes > o.getTotalViajes()){
			return 1;
		}else if(totalViajes < o.getTotalViajes()){
			return -1;
		}
		else
			return 0;
	}


}



