package model.vo;

public class VOBikeRoute implements Comparable<VOBikeRoute>{

	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------

	private String bikeRouteType;

	private String referenceStreet;

	private String endStreet1;

	private String endStreet2;

	private double length;
	
	private String id;
	
	private GeoCoordinate[] locationcoordinates;
	

	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------

	/*
	 * Constructor vacio
	 */
	public VOBikeRoute() {
		id = "";
		bikeRouteType = "";
		locationcoordinates = null;
		referenceStreet = "";
		endStreet1 = "";
		endStreet2 = "";
		length = 0;
	}

	/*
	 * Construye una ruta con los valores que se pasan por parametro
	 */
	public VOBikeRoute(String id, String bikeRouteType, GeoCoordinate[] locationcoordinates, String referenceStreet, String endStreet1, String endStreet2, double length ) {
		this.id = id;
		this.bikeRouteType = bikeRouteType;
		this.locationcoordinates = locationcoordinates;
		this.referenceStreet = referenceStreet;
		this.endStreet1 = endStreet1;
		this.endStreet2 = endStreet2;
		this.length = length;
	}

	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------


	@Override
	public int compareTo(VOBikeRoute arg0) {

		if(id.compareTo(arg0.id) > 0)
			return 1;
		else if(id.compareTo(arg0.id) < 0)
			return -1;
		else
			return 0;
	}

	//--------------------------------------------------------
	//Getter y Setters
	//--------------------------------------------------------


	public String getEndStreet2() {
		return endStreet2;
	}

	public void setEndStreet2(String endStreet2) {
		this.endStreet2 = endStreet2;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public String getEndStreet1() {
		return endStreet1;
	}

	public void setEndStreet1(String endStreet1) {
		this.endStreet1 = endStreet1;
	}

	public String getReferenceStreet() {
		return referenceStreet;
	}

	public void setReferenceStreet(String referenceStreet) {
		this.referenceStreet = referenceStreet;
	}

	public GeoCoordinate[] getRoute() {
		return locationcoordinates;
	}

	public void setRoute(GeoCoordinate[] coords) {
		this.locationcoordinates = coords;
	}

	public String getBikeRouteType() {
		return bikeRouteType;
	}

	public void setBikeRouteType(String bikeRouteType) {
		this.bikeRouteType = bikeRouteType;
	}
	
	public GeoCoordinate getFirstCoord() {
		return locationcoordinates[0];
	}

	public GeoCoordinate getLastCoord() {
		return locationcoordinates[locationcoordinates.length-1];
	}

	@Override
	public String toString() {
		
		String coordenadas = "";
		
		for(GeoCoordinate g : locationcoordinates) {
			coordenadas += "["+g.toString() + "] ";
		}
		
		String string = "ID: " +id + " | Type: " + bikeRouteType + " | Location: " + coordenadas
				+ " | RefereceStreet: " + referenceStreet + " | EndSreet1: " + endStreet1
				+ " | EndStreet2: " +endStreet2 + " | Lenght: " +length;
		
		return string;
	}
}
