package model.logic;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Iterator;


import api.IDivvyTripsManager;
import controller.Controller.ResultadoCampanna;
import model.vo.VOBike;
import model.vo.VOBikeRoute;
import model.vo.VOSector;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.HashTable;
import model.data_structures.MaxHeap;
import model.data_structures.Queue;
import model.data_structures.RedBlackTree;
import model.data_structures.Stack;



public class DivvyTripsManager implements IDivvyTripsManager {

	//--------------------------------------------------------
	//Datos de la red de ciclorutas
	//--------------------------------------------------------

	/**
	 * Stack de stations
	 */
	private Stack<VOStation> stationsS;

	/**
	 * Stack de trips
	 */
	private Stack<VOTrip> tripsS;

	/**
	 * Lista de rutas
	 */
	private DoublyLinkedList<VOBikeRoute> routes;

	/**
	 * Arbol Rojo Negro de bicicletas, la llave es la duracion de viajes total.
	 */
	private RedBlackTree<Integer, DoublyLinkedList<VOBike>> bikesRBT;

	/**
	 * Hash de sectores de la ciudad
	 */
	private HashTable <Integer, VOSector> hashSectores;

	/**
	 * HashTable 
	 */
	private HashTable<String, RedBlackTree <Integer, DoublyLinkedList<VOTrip> >> hashTable2bY3b;

	/**
	 * HashTable de estaciones indexados por id
	 */
	private HashTable<Integer, VOStation> hashTableStations;

	/**
	 * Arbol de listas de viajes indexados por fecha de terminacion
	 */
	private RedBlackTree<Date, DoublyLinkedList<VOTrip>> tripsTreeByEndDate;
	/**
	 * HashTable de viajes indexados por duracion 
	 */
	private HashTable<Integer, DoublyLinkedList<VOTrip>> hashTableTrips;

	/**
	 * HashTable de viajes indexados por duracion 
	 */
	private MaxHeap<VOTrip> heapTripsByDistance;

	private DoublyLinkedList<VOBike> bikesTotal;


	//--------------------------------------------------------
	//Clases auxiliares del manager
	//--------------------------------------------------------

	/*
	 * Administrador de Datos
	 */
	private DataSubManager dataManager;

	/*
	 * Administrador de consutas sobre viajes
	 */
	private	TripsSubManager tripsM;

	/*
	 * Administrador de consutas sobre bicicletas
	 */
	private	BikesSubManager bikesM;

	/*
	 * Administrador de consutas sobre estaciones
	 */
	private	StationsSubManager stationsM;


	/*
	 * Administrador de consutas sobre ciclorutas
	 */
	private	BikeRoutesSubManager bikeRoutesM;

	/*
	 * Administrador de consutas sobre sectores
	 */
	private	SectorsSubManager sectoresM;

	/*
	 * Bordes externos de los sectores.
	 */
	private VOSector cuadranteCiudad;

	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------
	public DivvyTripsManager() {
		super();
		stationsS= new Stack<VOStation>();
		//( del mas viejo almas reciente)
		tripsS=new Stack<VOTrip>();
		routes=new DoublyLinkedList<VOBikeRoute>();

		dataManager = new DataSubManager();
		tripsM = new TripsSubManager();
		stationsM = new StationsSubManager();
		tripsM = new TripsSubManager();
		bikesM = new BikesSubManager();
		bikeRoutesM = new BikeRoutesSubManager();
		sectoresM = new SectorsSubManager();

	}

	//==========================================================
	//METODOS DE CARGA
	//==========================================================

	/**
	 * Carga los viajes
	 * @param  ubicacion del archivo.
	 */
	@Override
	public void loadTrips(String tripsFile) {
		if(tripsFile.equals(""))
			tripsS = new Stack<VOTrip>();
		else
			tripsS = dataManager.loadTrips(tripsFile, tripsS);
	}

	/**
	 * Carga las estaciones
	 * @param  ubicacion del archivo.
	 */
	@Override
	public void loadStations(String stationsFile) {
		if(stationsFile.equals(""))
			stationsS = new Stack<VOStation>();
		else
			stationsS = dataManager.loadStations(stationsFile, stationsS);
	}

	/**
	 * Carga las ciclorutas de las bicicletas, las rutas deben cargarse en una lista
	 * en el orden que aparecen en el archivo.
	 * @param bikeRoutesFile ubicacion del archivo.
	 */

	public void loadBikeRoutesJSON(String jsonRoute) {
		routes= dataManager.loadBikeRoutesJSON(jsonRoute);
		cuadranteCiudad = darCuadranteLimiteCiudad();
		hashSectores=sectoresM.crearHashSectores(cuadranteCiudad);
		sectoresM.cargarCiclorutasYEstacionesASectores(hashSectores, routes,stationsS, cuadranteCiudad);
	}

	public void loadBikes() {

		VOTrip[] tripsArr= tripsS.toArray(VOTrip.class);
		bikesTotal =bikesM.getBikesListFromTrips(tripsArr, stationsS );
	}



	/**
	 * Carga las bicicletas en un arbol rojo negro
	 * Pre: Las estaciones y las bicletas deben estar cargadas
	 */
	/*public void loadBikesRBT() {

		VOTrip[] tripsArr= new VOTrip[tripsS.size()];
		int i=0;
		Iterator<VOTrip> iter = tripsS.iterator();

		while(iter.hasNext()) {
			tripsArr[i]= iter.next();
			i++;
		}

		setBikesRBT(bikesM.getBikesRBTreeFromTrips(tripsArr,  stationsS ));
	}*/


	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE A 
	//==========================================================

	/**
	 * Buscar los viajes que terminaron en una estaci�n con capacidad mayor o igual 
	 * a �n� dada la fecha �d� (d�a/mes/a�o) de terminaci�n.
	 * @param capacidad de la estaci�n
	 * @param fechaTerminacion
	 * @return Una lista DoublyLinkedList con los viajes que cumplen con los requerimientos o null si ninguno cumple
	 */
	public DoublyLinkedList<VOTrip> A1(int capacidad, LocalDate fechaTerminacion) {
		return tripsM.req1A(tripsTreeByEndDate, hashTableStations, fechaTerminacion, capacidad);
	}

	/**
	 * Buscar los viajes que tienen duraciones similares
	 * @param minutos 
	 * @return Una lista encadenada con los viajes que pertenezcan al rango de duraci�n
	 */
	public DoublyLinkedList<VOTrip> A2(int minutos) {
		return tripsM.requ2A(hashTableTrips, minutos);
	}

	/**
	 * Busca los n viajes con los recorridos m�s largos en una fecha (d�a/mes/a�o) y
	 * retorna una lista DoublyLinkedList con los n viajes que se encuentra en esa fecha y tienen la mayor 
	 * distancia recorrida ordenados de mayor a menor por distancia
	 * @param n
	 * @param fecha
	 * @return 
	 */
	public DoublyLinkedList<VOTrip> A3(int n, LocalDate fecha) {
		return tripsM.requ3A(heapTripsByDistance, n, fecha);
	}

	public void cargarEstructurasParteA() {

		hashTableStations = new HashTable<Integer, VOStation>(500);
		tripsTreeByEndDate = new RedBlackTree<Date, DoublyLinkedList<VOTrip>>();
		hashTableTrips = new HashTable<Integer, DoublyLinkedList<VOTrip>>(1000);
		heapTripsByDistance = new MaxHeap<VOTrip>(tripsS.size());

		tripsTreeByEndDate = tripsM.darArboldeListasDeViajesIndexadosPorFechaTerminacion(tripsS, tripsTreeByEndDate);
		cargarHashEstaciones();
		hashTableTrips = tripsM.darHashViajesIndexadosPorDuracion(hashTableTrips, tripsS);
		heapTripsByDistance = tripsM.darHeapViajesOrdenadosPorDistancia(heapTripsByDistance, tripsS, hashTableStations);

	}

	private void cargarHashEstaciones() {
		for(VOStation s: stationsS) {
			hashTableStations.put(s.darId(), s);
		}
	}

	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE B 
	//==========================================================

	// Req 1B
	public DoublyLinkedList<VOBike> buscarBicicletasEnRangoDuracionViajes(int min, int max) {

		//la lista de respuesta
		DoublyLinkedList<VOBike> respuesta=new DoublyLinkedList<VOBike>();
		//se inicializa arbol rojo negro de bicicletas.
		RedBlackTree< Integer , DoublyLinkedList<VOBike>> bikesBRT = bikesM.getBikesRBTreeFromTrips(bikesTotal);

		Iterator<Integer> iter=bikesBRT.keysInRange(min, max);

		while (iter.hasNext()) {
			Integer llaveBuscada = (Integer) iter.next();
			respuesta.addAll(respuesta.getSize(),bikesBRT.get(llaveBuscada));
		}

		return respuesta;
	}

	public DoublyLinkedList<VOTrip> viajesPorEstacionesDeSalidaYLlegadaEnUnRangoDeTiempo(String StationStart, String StationEnd, int limiteInferiorTiempo, int limiteSuperiorTiempo) {

		System.out.println("entra al metodo");

		DoublyLinkedList<VOTrip> respuesta=new DoublyLinkedList <VOTrip> ();

		String key1= StationStart+"-"+StationEnd;

		System.out.println(hashTable2bY3b.get(key1).height());

		Iterator<Integer> iter = hashTable2bY3b.get(key1).keysInRange(limiteInferiorTiempo, limiteSuperiorTiempo);

		while (iter.hasNext()) {
			Integer llaveBuscada =  iter.next();
			respuesta.addAll(respuesta.getSize(),hashTable2bY3b.get(key1).get(llaveBuscada));
		}

		return respuesta;

	}
	public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		String llave=estacionDeInicio+"-"+estacionDeLlegada;

		int[] horas=new int[24];

		RedBlackTree <Integer, DoublyLinkedList<VOTrip> > arbol = hashTable2bY3b.get(llave);

		Iterator<DoublyLinkedList<VOTrip>> iter=arbol.valuesIterator();
		while (iter.hasNext()) {
			//todos los arboles en la lista tienen la misma duracion
			//hay tener una cuenta de cuantos trips hay con la misma duracion
			DoublyLinkedList<VOTrip> trips= iter.next();

			for (VOTrip voTrip : trips) {
				horas[voTrip.getStart_time().getHour()]++;
			}
		}
		return horas;
	}



	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE C 
	//==========================================================

	public ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna ){
		MaxHeap<VOStation> heapMax = new MaxHeap<VOStation>(stationsS.size());
		ResultadoCampanna resultado=new ResultadoCampanna();
		LocalDateTime inicial=DataSubManager.convertirFecha_Hora_LDT((mesCampanna-1)+"/01/2017" , "00:00:00");
		LocalDateTime finalF=DataSubManager.convertirFecha_Hora_LDT((mesCampanna+1)+"/01/2017" , "00:00:00");
		Queue<VOTrip> tripsX = getTripsBetweenDates(inicial, finalF);
		for (VOStation station : stationsS) {
			for (VOTrip voTrip : tripsX) {

				boolean esFromStation=station.darId()==voTrip.getFrom_station_id();
				boolean esToStation=station.darId()==voTrip.getTo_station_id();

				if(esToStation && esFromStation) {
					station.increasePuntosPublicidad();
					station.increasePuntosPublicidad(); 
					if(voTrip.getUsertype().equals("Subscriber")) {
						station.increasePuntosPublicidad();
					}
				}else if(esFromStation) {
					station.increasePuntosPublicidad();
					if(voTrip.getUsertype().equals("Subscriber")) {
						station.increasePuntosPublicidad();
					}
				}else if(esToStation) {
					station.increasePuntosPublicidad();
					if(voTrip.getUsertype().equals("Subscriber")) {
						station.increasePuntosPublicidad();
					}
				}

			}
		}

		for (VOStation station1 : stationsS) {
			try {
				heapMax.agregar(station1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < numEstacionesConPublicidad; i++) {
			resultado.estaciones.add(heapMax.max());
		}

		resultado.costoTotal=(int) (numEstacionesConPublicidad*valorPorPunto);

		return resultado;
	}

	public double[] C2(int LA, int LO){
		// TODO Auto-generated method stub
		return null;
	}
	public DoublyLinkedList<VOBikeRoute>  busquedaGeorreferenciadaDeCiclorutasC4(double latitud, double longitud) {
		int numSector=sectoresM.sectorDelPunto(longitud, latitud, cuadranteCiudad);
		return hashSectores.get(numSector).getCicloRutasPorSector(); 
	}

	public DoublyLinkedList<VOStation> busquedaGeorreferenciadaDeEstacionesC3(double latitud, double longitud) {
		int numSector=sectoresM.sectorDelPunto(longitud, latitud, cuadranteCiudad);
		return hashSectores.get(numSector).getEstacionesPorSector();
	}




	public DoublyLinkedList<VOStation> C3(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}
	public DoublyLinkedList<VOBikeRoute> C4(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}
	public DoublyLinkedList<String> C5(double latitudI, double longitudI, double latitudF, double longitudF){
		VOSector sectorInicial = hashSectores.get(sectorDelPunto(longitudI, latitudI));
		VOSector sectorFinal = hashSectores.get(sectorDelPunto(longitudF, latitudF));

		DoublyLinkedList<VOBikeRoute> listaRutas = bikeRoutesM.darCicloRutasConCoordsEnAmbosSectores(sectorInicial, sectorFinal);
		return bikeRoutesM.req5C(latitudI, longitudI, latitudF, longitudF, sectoresM, cuadranteCiudad, listaRutas);
	}


	//==========================================================
	//OTROS/AUXILIARES
	//==========================================================

	public Queue <VOTrip> getTripsBetweenDates (LocalDateTime initialDateP, LocalDateTime finalDateP) {
		return tripsM.getTripsBetweenDates(initialDateP, finalDateP, tripsS);
	}

	public int sectorDelPunto(double lon, double lat) {
		return sectoresM.sectorDelPunto(lon, lat, cuadranteCiudad);
	}

	public String longsYLatsMayMin() {
		return "Longitud m�xima: "+ sectoresM.mayorLongitudRutas(routes) +", longitud m�nima: "+ sectoresM.menorLongitudRutas(routes) +", latitud m�xima: "+ sectoresM.mayorLatitudRutas(routes) +" y latitud m�nima: "+ sectoresM.menorLatitudRutas(routes);
	}

	public VOSector darCuadranteLimiteCiudad() {

		VOSector limites = new VOSector();

		Double mayorLongRoutes=sectoresM.mayorLongitudRutas(routes);
		Double mayorLongStations=sectoresM.mayorLongitudEstaciones(stationsS);

		Double mayorLatRoutes=sectoresM.mayorLatitudRutas(routes);
		Double mayorLatStations=sectoresM.mayorLatitudEstaciones(stationsS);


		Double menorLongRoutes=sectoresM.menorLongitudRutas(routes);
		Double menorLongStations=sectoresM.menorLongitudEstaciones(stationsS);

		Double menorLatRoutes=sectoresM.menorLatitudRutas(routes);
		Double menorLatStations=sectoresM.menorLatitudEstaciones(stationsS);

		limites.setMaxLatitud((mayorLatRoutes > mayorLatStations) ? mayorLatRoutes : mayorLatStations);
		limites.setMaxLongitud((mayorLongRoutes > mayorLongStations)? mayorLongRoutes : mayorLongStations);
		limites.setMinLatitud((menorLatRoutes < menorLatStations) ?  menorLatRoutes : menorLatStations);
		limites.setMinLongitud((menorLongRoutes < menorLongStations) ?  menorLongRoutes : menorLongStations);


		return limites;

	}
	public void cargarhashTable2bY3b() {
		hashTable2bY3b=new HashTable<String, RedBlackTree <Integer, DoublyLinkedList<VOTrip> >>(tripsS.size());

		for (VOTrip voTrip : tripsS) {
			System.out.println(hashTable2bY3b.sizeKeys());
			String llave=voTrip.getFrom_station_name()+"-"+voTrip.getTo_station_name();
			RedBlackTree<Integer, DoublyLinkedList<VOTrip> > existe = hashTable2bY3b.get( llave );


			//si el valor de la llave buscada en el hash en null o no existe la llave en el hash
			if( existe==null ){
				// se crea una nueva lista con el viaje actual
				DoublyLinkedList<VOTrip> nueva=new DoublyLinkedList<VOTrip>();
				nueva.add(voTrip);
				//se crea un nuevo arbol
				RedBlackTree <Integer, DoublyLinkedList<VOTrip> > RBT= new RedBlackTree <Integer, DoublyLinkedList<VOTrip> >();

				// se anade la lista nueva al arbol nuevo (llave=duracion del viaje, value=lista de viajes)
				RBT.put(voTrip.getTripSeconds(), nueva);
				// se anade el arbol nuevo al hash (llave=estaciones llegada y salida, value=arbolRB)
				hashTable2bY3b.put(llave, RBT);

			}else{
				//si el valor de la llave en el arbol es null o la llave no existe
				if(existe.get(voTrip.getTripSeconds())==null){
					// se crea una lista nueva con el viaje actual
					DoublyLinkedList<VOTrip> nueva1=new DoublyLinkedList<VOTrip>();
					nueva1.add(voTrip);
					// se anade la lista al arbol (llave=duracion del viaje, value=lista de viajes)
					existe.put(voTrip.getTripSeconds(), nueva1);
				}else {
					//si el hash contiene un arbol con una lista que tenga la llave (duracion del viaje) solo se anade
					//el viaje a esa lista.
					existe.get(voTrip.getTripSeconds()).add(voTrip);
				}
			}
		}
	}
	
	public double distance(double startLat, double startLong,
			double endLat, double endLong) {
		return bikesM.distance(startLat, startLong, endLat, endLong);
	}



	//--------------------------------------------------------
	//Getters
	//--------------------------------------------------------


	public int getTripsSSize(){
		return tripsS.size();
	}

	public Stack <VOStation> darStationsS(){
		return stationsS;
	}

	public DoublyLinkedList<VOBikeRoute> getBikeRoutes(){
		return routes;
	}

	public int getBikesTotalSize() {
		return bikesTotal.getSize();
	}

	
	public VOSector getCuadranteCiudad() {
		return cuadranteCiudad;
	}



}
