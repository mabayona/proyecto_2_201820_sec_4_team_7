package model.logic;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import api.ITrips;
import model.data_structures.DoublyLinkedList;
import model.data_structures.HashTable;
import model.data_structures.MaxHeap;
import model.data_structures.Queue;
import model.data_structures.RedBlackTree;
import model.data_structures.Stack;
import model.vo.VOSector;
import model.vo.VOStation;
import model.vo.VOTrip;

public class TripsSubManager implements ITrips{

	private static final int EARTH_RADIUS = 6371;

	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE A 
	//==========================================================


	//----------------------------------------------------------
	//Parte 1A
	//----------------------------------------------------------


	public DoublyLinkedList<VOTrip> req1A(RedBlackTree<Date, DoublyLinkedList<VOTrip>> tripsTree, HashTable<Integer, VOStation> hashTableStations, LocalDate localDateFin, int capacidad){
		DoublyLinkedList<VOTrip> retornar = new DoublyLinkedList<VOTrip>();
		DoublyLinkedList<VOTrip> listaViajesFecha = darViajesEnFecha(tripsTree, localDateFin);
		if(listaViajesFecha == null) {
			return null;
		}
		else {
			VOStation temp;
			for(VOTrip t : listaViajesFecha) {
				temp = hashTableStations.get(t.getTo_station_id());
				if(temp.darDpcapacity() >= capacidad)
					retornar.add(t);
			}

			if(retornar.isEmpty())
				return null;
			else
				return retornar;
		}
	}


	private DoublyLinkedList<VOTrip> darViajesEnFecha(RedBlackTree<Date, DoublyLinkedList<VOTrip>> tripsTree, LocalDate localD) {
		DateFormat formart = new SimpleDateFormat("dd/MM/yyyy");
		Date llave = null;
		try {
			llave = formart.parse(localD.getDayOfMonth() + "/" + localD.getMonthValue() + "/" + localD.getYear());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return tripsTree.get(llave);
	}


	public RedBlackTree<Date, DoublyLinkedList<VOTrip>> darArboldeListasDeViajesIndexadosPorFechaTerminacion(Stack<VOTrip> tripsS, RedBlackTree<Date, DoublyLinkedList<VOTrip>> tripsTree){

		DateFormat formart = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		LocalDateTime localD;

		for(VOTrip trip : tripsS) {

			localD = trip.getEnd_time();
			try {
				date = formart.parse(localD.getDayOfMonth() + "/" + localD.getMonthValue() + "/" + localD.getYear());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(tripsTree.contains(date)) { //si ya contiene esa fecha
				tripsTree.get(date).add(trip); //busca la lista y le a�ade el nuevo viaje
			}
			else {
				DoublyLinkedList<VOTrip> nuevo = new DoublyLinkedList<VOTrip>(); // crea una nueva lista
				nuevo.add(trip); //coloca el viaje en la lista
				tripsTree.put(date, nuevo); // y a�ade la lista al arbol
			}
		}
		return tripsTree;

	}

	//----------------------------------------------------------
	//Parte 2A
	//----------------------------------------------------------

	public DoublyLinkedList<VOTrip> requ2A(HashTable<Integer, DoublyLinkedList<VOTrip>> hashTableTrips, int minutos){
		DoublyLinkedList<VOTrip> listaBuscada = null;

		int llave = (minutos % 2) == 0 ? minutos : minutos +1;
		listaBuscada = hashTableTrips.get(llave);
		return listaBuscada;
	}

	public HashTable<Integer, DoublyLinkedList<VOTrip>> darHashViajesIndexadosPorDuracion(HashTable<Integer, DoublyLinkedList<VOTrip>> hashTableTrips, Stack<VOTrip> tripsS){
		DoublyLinkedList<VOTrip> listaGrupo = null;
		for(VOTrip trip : tripsS) {
			int segundos = trip.getTripSeconds();
			int llave = 2;
			if(segundos > 0)
				llave = darNumeroEnMinutosDeGrupo(segundos);

			listaGrupo = hashTableTrips.get(llave);
			if(listaGrupo != null)
				listaGrupo.add(trip);
			else {
				listaGrupo = new DoublyLinkedList<VOTrip>();
				listaGrupo.add(trip);
				hashTableTrips.put(llave, listaGrupo);
			}
		}
		return hashTableTrips;
	}


	private int darNumeroEnMinutosDeGrupo(int segundos) {

		double minutos = (double)(segundos) / 60.0;

		double parteEntera = Math.floor(minutos);
		double parteDecimal = minutos - parteEntera;

		boolean esPar = (parteEntera % 2) == 0;

		if(esPar && parteDecimal == 0.0)
			return (int)parteEntera;
		else if(esPar && parteDecimal != 0.0) 
			return (int) (parteEntera + 2.0);
		else
			return (int) parteEntera + 1;
	}

	//----------------------------------------------------------
	//Parte 3A
	//----------------------------------------------------------

	public DoublyLinkedList<VOTrip> requ3A(MaxHeap<VOTrip> heapTripsByDistance, int n, LocalDate fecha){
		DoublyLinkedList<VOTrip> retornar = new DoublyLinkedList<VOTrip>();
		MaxHeap<VOTrip> copy = clonarHeap(heapTripsByDistance);

		int i = n;
		VOTrip tempTrip;
		LocalDate tempDateStart;
		LocalDate tempDateEnd;
		while(i > 0) {
			tempTrip = copy.max();
			tempDateEnd = tempTrip.getEnd_time().toLocalDate();
			tempDateStart = tempTrip.getStart_time().toLocalDate();

			if(tempDateEnd.compareTo(fecha) == 0 || tempDateStart.compareTo(fecha) == 0) {
				retornar.add(tempTrip);
				i--;
			}
		}
		if(retornar.isEmpty())
			return null;
		else
			return retornar;
	}

	private MaxHeap<VOTrip> clonarHeap(MaxHeap<VOTrip> heapTripsByDistance) {
		MaxHeap<VOTrip> retornar = new MaxHeap<VOTrip>(heapTripsByDistance.darNumElementos());
		int i = heapTripsByDistance.darNumElementos();
		while(i > 0) {
			try {
				retornar.agregar(heapTripsByDistance.max());
				i--;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return retornar;
	}


	public MaxHeap<VOTrip> darHeapViajesOrdenadosPorDistancia(MaxHeap<VOTrip> heapTripsByDistance, Stack<VOTrip> tripsS, HashTable<Integer, VOStation> hashTableStations){

		VOStation stationFromTemp = null;
		VOStation stationToTemp = null;

		for(VOTrip trip : tripsS) {

			stationFromTemp = hashTableStations.get(trip.getFrom_station_id());
			stationToTemp = hashTableStations.get(trip.getTo_station_id());
			double distanceAdd = this.distance(stationFromTemp.darLatitude(), stationFromTemp.darLongitude(), stationToTemp.darLatitude(), stationToTemp.darLongitude());
			trip.setDistance(distanceAdd);

			try {
				heapTripsByDistance.agregar(trip);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return heapTripsByDistance;
	}


	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE B 
	//==========================================================



	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE C 
	//==========================================================


	//==========================================================
	//OTROS/AUXILIARES
	//==========================================================

	public double distance(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c; // <-- d
	}


	private static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}

	public Queue<VOTrip> getTripsBetweenDates(LocalDateTime initialDateP, LocalDateTime finalDateP, Stack<VOTrip> tripsS) {
		Queue <VOTrip> respuesta=new Queue<VOTrip>();

		for(VOTrip tempTrip : tripsS) {
			if(tempTrip.getStart_time().isAfter(initialDateP) && tempTrip.getEnd_time().isBefore(finalDateP)) 
			{
				respuesta.enqueue(tempTrip);
			}
		}
		return respuesta;
	}

}
