package model.logic;

import java.util.LinkedList;

import model.data_structures.DoublyLinkedList;
import model.vo.GeoCoordinate;
import model.vo.VOBikeRoute;
import model.vo.VOSector;

public class BikeRoutesSubManager {
	
	private static final int EARTH_RADIUS = 6371;


	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE A 
	//==========================================================
	
	
	
	
	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE B 
	//==========================================================



	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE C 
	//==========================================================
	
	public DoublyLinkedList<String> req5C(double latitudI, double longitudI, double latitudF, double longitudF, SectorsSubManager sectoresM, VOSector cuadranteCiudad, DoublyLinkedList<VOBikeRoute> listaRutas) {
		DoublyLinkedList<String> mensajeRetornar = new DoublyLinkedList<String>();
		GeoCoordinate inicial = new GeoCoordinate(longitudI, latitudI);
		GeoCoordinate finalC = new GeoCoordinate(longitudF, latitudF);
		
		GeoCoordinate g1;
		GeoCoordinate g2;
		
		double dis1; //distancia mas cercana al punto incial
		double dis2; //distancia mas cercana al punto final
		double dis3; //distancia entre las dos coordenadas dis1 y dis2
		for(VOBikeRoute b : listaRutas) {
			g1 = this.darCoordenadaMasCercanaAXCoordenadaEnMismoSector(b.getRoute(), inicial, sectoresM, cuadranteCiudad);
			g2 = this.darCoordenadaMasCercanaAXCoordenadaEnMismoSector(b.getRoute(), finalC, sectoresM, cuadranteCiudad);
			dis1 = distance(inicial.getLatitud(), inicial.getLongitud(), g1.getLatitud(), g1.getLongitud());
			dis2 = distance(finalC.getLatitud(), finalC.getLongitud(), g2.getLatitud(), g2.getLongitud());
			dis3 = distance(g1.getLatitud(), g1.getLongitud(), g2.getLatitud(), g2.getLongitud());
			double suma = dis1 + dis2 + dis3;
			
			String mensaje = "Calle de referencia: "+ b.getReferenceStreet() + "\n" + "Distancia total: "
			+ dis1 + " + " + dis2 +" + "+ dis3 + " = " + suma;
			
			mensajeRetornar.add(mensaje);
		}
		
		return mensajeRetornar;

	}



	
	/**
	 * Retorna la distacia  m�s cercana entre 1 coordenada y una lista de coordeandas
	 * @return
	 */
	private GeoCoordinate darCoordenadaMasCercanaAXCoordenadaEnMismoSector(GeoCoordinate[] locationcoordinates, GeoCoordinate geoX, SectorsSubManager sectoresM, VOSector cuadranteCiudad) {
		LinkedList<GeoCoordinate> coordeandasEnSector = new LinkedList<GeoCoordinate>();
		int numSector = sectoresM.sectorDelPunto(geoX.getLongitud(), geoX.getLatitud(), cuadranteCiudad);
		int numSectorTemp;
		for(GeoCoordinate g : locationcoordinates) {
			numSectorTemp = sectoresM.sectorDelPunto(g.getLongitud(), g.getLatitud(), cuadranteCiudad);
			if(numSector == numSectorTemp)
				coordeandasEnSector.add(g);
		}
		double min = Double.MAX_VALUE;
		double actual = 0.0;
		GeoCoordinate geo = null;
		for(GeoCoordinate o : coordeandasEnSector) {
			actual = distance(geoX.getLatitud(), geoX.getLongitud(), o.getLatitud(), o.getLongitud());
			if(actual < min) {
				min = actual;
				geo = o;
			}
		}
		return geo;
	}
	

	/**
	 * Retorna la lista de ciclorutas que tiene coordenadas en ambos sectores, incial y final
	 * @param sectorInicial
	 * @param sectorFinal
	 * @return
	 */
	public DoublyLinkedList<VOBikeRoute> darCicloRutasConCoordsEnAmbosSectores(VOSector sectorInicial, VOSector sectorFinal){
		DoublyLinkedList<VOBikeRoute> listaRetornar = new DoublyLinkedList<VOBikeRoute>();
		
		if(sectorInicial == null || sectorFinal == null)
			return null;
		
		DoublyLinkedList<VOBikeRoute> listaSector1 = sectorInicial.getCicloRutasPorSector();
		DoublyLinkedList<VOBikeRoute> listaSector2 = sectorFinal.getCicloRutasPorSector();
		for(VOBikeRoute r : listaSector1) {
			for(VOBikeRoute r2 : listaSector2) {
				if(r.compareTo(r2) == 0) {
					listaRetornar.add(r);
					break;
				}
			}
		}
		if(listaRetornar.isEmpty())
			return null;
		else
		return listaRetornar;
		
	}
	
	//==========================================================
	//OTROS/AUXILIARES
	//==========================================================
	
	public double distance(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c; // <-- d
	}


	private static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}



}
