package model.logic;

import model.data_structures.DoublyLinkedList;
import model.data_structures.HashTable;
import model.data_structures.Stack;
import model.vo.GeoCoordinate;
import model.vo.VOBikeRoute;
import model.vo.VOSector;
import model.vo.VOStation;

public class SectorsSubManager {



	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE A 
	//==========================================================




	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE B 
	//==========================================================



	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE C 
	//==========================================================


	//==========================================================
	//OTROS/AUXILIARES
	//==========================================================

	public HashTable<Integer, VOSector> crearHashSectores(VOSector cuadranteCiudad) {

		HashTable<Integer, VOSector> sectoresProbing=new HashTable<Integer, VOSector>(100);

		double longMaxGlobal=cuadranteCiudad.getMaxLongitud();
		double longMinGlobal=cuadranteCiudad.getMinLongitud();
		double diferenciaLong = longMaxGlobal-longMinGlobal;

		double latMaxGlobal=cuadranteCiudad.getMaxLatitud();
		double latMinGlobal=cuadranteCiudad.getMinLatitud();
		double diferenciaLat=latMaxGlobal-latMinGlobal;

		int idCuenta=0;

		for (int i = 0; i < 10; i++) {

			//el maximo en latitud del sector, limite superior
			double latFinalSector = latMaxGlobal-(Double.valueOf(i)*(diferenciaLat/10));
			//el minimo en latitud del sector, limite inferior del sector
			double latInicialSector = latMaxGlobal - (Double.valueOf(i+1)*(diferenciaLat/10));

			for (int j = 0; j < 10; j++) {

				double longMinSector = longMinGlobal + (Double.valueOf(j)*(diferenciaLong/10));
				double longMaxSector = longMinGlobal + (Double.valueOf(j+1)*(diferenciaLong/10));
				idCuenta++;
				sectoresProbing.put(idCuenta, new VOSector(idCuenta, longMaxSector, longMinSector, latFinalSector, latInicialSector));

			}
		}
		return sectoresProbing;
	}





	public void cargarCiclorutasYEstacionesASectores(HashTable<Integer, VOSector> sectoresProbing, DoublyLinkedList<VOBikeRoute> routes,Stack <VOStation> estaciones, VOSector cuadranteCiudad){

		for (VOBikeRoute voBikeRoute : routes) {

			GeoCoordinate[] temp = voBikeRoute.getRoute();

			double lat1 = 0;
			double lon1 = 0;
			int numSector= 0;
			VOSector sectorDelPunto = new VOSector();
			// recorre el arreglo de puntos geograficos de la rutaX
			for (int j = 0; j < temp.length; j++) {
				lat1=  temp[j].getLatitud();
				lon1=  temp[j].getLongitud();
				numSector= sectorDelPunto( lon1, lat1, cuadranteCiudad );

				// encuentra el sector al que el punto pertenece (sectorX)

				sectorDelPunto= sectoresProbing.get(numSector);

				// anade la ruta a la lista de rutas del sectorX si no esta anadida ya.
				if(!sectorDelPunto.getCicloRutasPorSector().contains(voBikeRoute)){
					sectorDelPunto.getCicloRutasPorSector().add(voBikeRoute);


				}
			}
		}
		for (VOStation voStation : estaciones) {

			double lat1 = voStation.darLatitude();
			double lon1 = voStation.darLongitude();
			int numSector= 0;
			VOSector sectorDelPunto = new VOSector();

			numSector= sectorDelPunto( lon1, lat1, cuadranteCiudad );

			// encuentra el sector al que el punto pertenece (sectorX)

			sectorDelPunto= sectoresProbing.get(numSector);

			// anade la ruta a la lista de rutas del sectorX si no esta anadida ya.
			if(!sectorDelPunto.getEstacionesPorSector().contains(voStation)){
				sectorDelPunto.getEstacionesPorSector().add(voStation);
			}		
		}

	}

	public int sectorDelPunto(double longitudP, double latitudP, VOSector cuadranteCiudad){

		double yf= cuadranteCiudad.getMinLatitud();  //  menorLatitudRutas(routes)
		double yi=cuadranteCiudad.getMaxLatitud();	//mayorLatitudRutas(routes);
		double deltaY = yi-yf;
		double xf=cuadranteCiudad.getMaxLongitud();	//mayorLongitudRutas(routes);
		double xi=cuadranteCiudad.getMinLongitud();	//menorLongitudRutas(routes);
		double deltaX = xf - xi;

		int cuentaLat=0;
		int cuentaLon=0;
		//booleans para salirse de los fors
		boolean lat=false;
		boolean lon=false;

		//Empieza i en yi, que es latMayor y va restando hasta yf, que es latMenor
		for (double i = yi; i >= yf && !lat; i-=(deltaY/10)) {
			//empieza el el sector 1
			cuentaLat++; 
			//si el punto actual est� entre i e i-(deltaY/10), si el punto est� en los limites
			//queda en el sector m�s peque�o al que pertenece
			if(( i-(deltaY/10) <= latitudP) && (i >= latitudP)){
				lat=true;
			}
		}

		//Empieza i en xi, que es lonMenor y va sumando hasta xf, que es lonMayor
		for (double i = xi; i <= xf && !lon; i+=(deltaX/10)) {
			//empieza el el sector 1
			cuentaLon++; 
			//si el punto actual est� entre i e i+(deltaX/10), si el punto est� en los limites
			//queda en el sector m�s peque�o al que pertenece
			if(( longitudP <= i+(deltaY/10) ) && (longitudP >= i)){
				lon=true;
			}
		}
		return ((cuentaLat-1)*10)+cuentaLon;

	}

	public DoublyLinkedList<VOBikeRoute> bicicletasEnUnSector(double longitudP, double latitudP, HashTable<Integer, VOSector> hashTable , VOSector cuadranteCiudad){
		DoublyLinkedList<VOBikeRoute> bicicletasDelSector=new DoublyLinkedList<VOBikeRoute>();
		int numsector3=sectorDelPunto(longitudP, latitudP, cuadranteCiudad);
		VOSector sector3= hashTable.get(numsector3);
		bicicletasDelSector=sector3.getCicloRutasPorSector();
		return bicicletasDelSector;
	}



	public double mayorLongitudEstaciones(Stack<VOStation> stations) {
		double max=-100000000.0;
		for (VOStation voStation : stations) {
			if(voStation.darLongitude()>max) {
				max=voStation.darLongitude();
			}
		}
		return max;
	}

	public double menorLongitudEstaciones(Stack<VOStation> stations) {
		double min=Double.MAX_VALUE;
		for (VOStation voStation : stations) {
			if(voStation.darLongitude()<min) {
				min=voStation.darLongitude();
			}
		}
		return min;
	}

	public double mayorLongitudRutas(DoublyLinkedList<VOBikeRoute> routes) {
		double max=-100000000.0;
		for (VOBikeRoute voBikeRoute : routes) {
			if(darMayorLongitudRuta(voBikeRoute)>max) {
				max=darMayorLongitudRuta(voBikeRoute);
			}
		}
		return max;
	}

	public double menorLongitudRutas(DoublyLinkedList<VOBikeRoute> routes) {
		double min=Double.MAX_VALUE;
		for (VOBikeRoute voBikeRoute : routes) {
			if(darMenorLongitudRuta(voBikeRoute)<min) {
				min=darMenorLongitudRuta(voBikeRoute);
			}
		}
		return min;
	}

	public double mayorLatitudRutas(DoublyLinkedList<VOBikeRoute> routes) {
		double max=0;
		for (VOBikeRoute voBikeRoute : routes) {
			if(darMayorLatitudRuta(voBikeRoute)>max) {
				max=darMayorLatitudRuta(voBikeRoute);
			}
		}
		return max;
	}

	public double menorLatitudEstaciones(Stack<VOStation> stations) {
		double min=Double.MAX_VALUE;
		for (VOStation voStation : stations) {
			if(voStation.darLatitude()<min) {
				min=voStation.darLatitude();
			}
		}
		return min;
	}

	public double mayorLatitudEstaciones(Stack<VOStation> stations) {
		double max=0;
		for (VOStation voStation : stations) {
			if(voStation.darLatitude()>max) {
				max=voStation.darLatitude();
			}
		}
		return max;
	}
	public double menorLatitudRutas(DoublyLinkedList<VOBikeRoute> routes) {
		double min=Double.MAX_VALUE;
		for (VOBikeRoute voBikeRoute : routes) {
			if(darMenorLatitudRuta(voBikeRoute)<min) {
				min=darMenorLatitudRuta(voBikeRoute);
			}
		}
		return min;
	}
	private double darMayorLongitudRuta(VOBikeRoute pRuta) {
		double mayor=-1000000000000.0;
		for (int i = 0; i < pRuta.getRoute().length; i++) {
			GeoCoordinate temp = pRuta.getRoute()[i];
			if(temp.getLongitud() > mayor) {
				mayor = temp.getLongitud();
			}
		}
		return mayor;
	}

	private double darMenorLongitudRuta(VOBikeRoute pRuta) {
		double menor=Double.MAX_VALUE;
		for (int i = 0; i < pRuta.getRoute().length; i++) {
			GeoCoordinate temp = pRuta.getRoute()[i];
			if(temp.getLongitud() < menor) {
				menor = temp.getLongitud();
			}

		}
		return menor;
	}

	private double darMayorLatitudRuta(VOBikeRoute pRuta) {
		double mayor=-1000000000.01;
		for (int i = 0; i < pRuta.getRoute().length; i++) {
			GeoCoordinate temp = pRuta.getRoute()[i];
			if(temp.getLatitud() > mayor) {
				mayor = temp.getLatitud();
			}

		}
		return mayor;
	}

	private double darMenorLatitudRuta(VOBikeRoute pRuta) {
		double menor=Double.MAX_VALUE;
		for (int i = 0; i < pRuta.getRoute().length; i++) {
			GeoCoordinate temp = pRuta.getRoute()[i];
			if(temp.getLatitud() < menor) {
				menor = temp.getLatitud();
			}
		}
		return menor;
	}

}
