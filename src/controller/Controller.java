package controller;

import model.data_structures.Stack;
import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOBikeRoute;
import model.vo.VOSector;
import model.vo.VOStation;
import model.vo.VOTrip;

import java.time.LocalDate;
import java.time.LocalDateTime;

import api.IDivvyTripsManager;


public class Controller {
	private static DivvyTripsManager manager = new DivvyTripsManager();

	//==========================================================
	//METODOS AUXILIARES
	//==========================================================

	public static int tripsSSize() {
		return manager.getTripsSSize();
	}
	public static int getStationsSSize() {
		return manager.darStationsS().size();
	} 
	public static int getRoutesSize() {
		return manager.getBikeRoutes().getSize();
	}

	public static int bikesSize() {
		return 0;//manager.bikesSize();
	}


	public static void loadTrips(String trips) {
		manager.loadTrips(trips);
	}

	public static void loadStations(String stations) {
		manager.loadStations(stations);
	}

	public static void loadBikeRoutesJSON(String ruta) {
		manager.loadBikeRoutesJSON(ruta);
	}

	public static void loadHash2BY3B() {
		manager.cargarhashTable2bY3b();
	}

	public static void loadBikes() { //TODO
		manager.loadBikes();
	}

	public static int darSector(double latitud, double longitud) {
		return manager.sectorDelPunto(longitud, latitud);
	}
	public static void cargarEstructurasParteA() {
		manager.cargarEstructurasParteA();
	}


	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE A 
	//==========================================================


	public static DoublyLinkedList<VOTrip> A1(int n, LocalDate fechaTerminacion) {
		return manager.A1(n, fechaTerminacion);
	}

	public static DoublyLinkedList<VOTrip> A2(int n){
		return manager.A2(n);
	}

	public static DoublyLinkedList<VOTrip> A3(int n, LocalDate fecha) {
		return manager.A3(n, fecha);
	}

	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE B 
	//==========================================================


	public static DoublyLinkedList<VOBike> B1(int limiteInferior, int limiteSuperior) {
		return manager.buscarBicicletasEnRangoDuracionViajes(limiteInferior, limiteSuperior);
	}

	public static DoublyLinkedList<VOTrip> B2(String fechaInicial, String fechaFinal, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
		return manager.viajesPorEstacionesDeSalidaYLlegadaEnUnRangoDeTiempo(fechaInicial, fechaFinal, limiteInferiorTiempo, limiteSuperiorTiempo);
	}

	public static int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		int[]respuesta=new int[2];
		int [] arr=manager.B3(estacionDeInicio, estacionDeLlegada);
		int mayorNumeroViajes=0;
		int index=0;
		for (int i = 0; i < arr.length; i++) {
			if(arr[i]>mayorNumeroViajes) {
				mayorNumeroViajes=arr[i];
				index=i;
			}
		}
		respuesta[0]=index;
		respuesta[1]=mayorNumeroViajes;
		return respuesta;
	}


	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE C 
	//==========================================================

	public static ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		return manager.C1(valorPorPunto, numEstacionesConPublicidad, mesCampanna);
	}

	public static double[] C2(int LA, int LO) {
		return manager.C2(LA, LO);
	}

	public static class ResultadoCampanna{
		public int costoTotal;
		public DoublyLinkedList<VOStation> estaciones;
	}

	public static DoublyLinkedList<VOStation> C3(double latitud, double longitud) {
		return manager.busquedaGeorreferenciadaDeEstacionesC3(latitud, longitud);
	}

	public static DoublyLinkedList<VOBikeRoute> C4(double latitud, double longitud) {
		return manager.busquedaGeorreferenciadaDeCiclorutasC4(latitud, longitud);
	}

	public static DoublyLinkedList<String> C5(double latitudI, double longitudI, double latitudF, double longitudF) {
		return manager.C5(latitudI, longitudI, latitudF, longitudF);
	}

	public static class resultadoCampanna{
		public int costoTotal=0;
		public DoublyLinkedList<VOStation> estaciones= new DoublyLinkedList<VOStation>();
	}

	public static VOSector getCuadranteCiudad() {
		return manager.getCuadranteCiudad();
	}
	
	public static double distance(double startLat, double startLong,
			double endLat, double endLong) {
		return manager.distance(startLat, startLong, endLat, endLong);
	}


}