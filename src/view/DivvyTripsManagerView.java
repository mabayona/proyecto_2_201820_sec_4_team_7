package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Scanner;

import controller.Controller;
import controller.Controller.ResultadoCampanna;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.HashTable;
import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.logic.FilesRoutes;
import model.vo.GeoCoordinate;
import model.vo.VOBike;
import model.vo.VOBikeRoute;
import model.vo.VOSector;
import model.vo.VOStation;
import model.vo.VOTrip;

public class DivvyTripsManagerView {

	public static void main(String[] args){

		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		int option;
		int n;
		int limiteSuperior;
		int limiteInferior;

		boolean estacionesQ1Q2Cargadas = false;
		boolean estacionesQ3Q4Cargadas = false;

		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();
			option = linea.nextInt();
			n = 0;
			limiteInferior =0;
			limiteSuperior =0;
			switch(option)
			{

			case 0:  //Carga de datos

				String dataTrips = "";  // ruta del archivo de Trips
				String dataStations = ""; // ruta del archivo de Stations
				boolean reiniciarDatos = false;

				printMenuCargar();
				int tamanoDatos = linea.nextInt();
				switch (tamanoDatos)
				{
				case 1:
					dataTrips = FilesRoutes.TRIPS_Q1.getFileRoute();
					dataStations = FilesRoutes.STATIONS_Q1_Q2.getFileRoute();

					break;
				case 2:
					dataTrips = FilesRoutes.TRIPS_Q2.getFileRoute();
					dataStations = FilesRoutes.STATIONS_Q1_Q2.getFileRoute();

					break;
				case 3:
					dataTrips = FilesRoutes.TRIPS_Q3.getFileRoute();
					dataStations = FilesRoutes.STATIONS_Q3_Q4.getFileRoute();

					break;
				case 4:
					dataTrips = FilesRoutes.TRIPS_Q4.getFileRoute();
					dataStations = FilesRoutes.STATIONS_Q3_Q4.getFileRoute();

					break;
				case 5: // Opcion para reiniciar los datos del sistema. Conjunto vacio de trips y de estaciones.
					dataTrips = "";
					dataStations = "";
					reiniciarDatos = true;
					break;
				}

				if (!reiniciarDatos)
				{
					System.out.println("Viajes que se cargaran al sistema: " + dataTrips);
					System.out.println("Estaciones que se cargaran al sistema: " + dataStations);
					System.out.println("==================================================================");

				}
				else {
					System.out.println("Se reiniciar�n los datos del sistema:");
					System.out.println("==================================================================");
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.currentTimeMillis();

				//CARGA LAS CICLORUTAS SOLO UNA VEZ
				if(Controller.getRoutesSize()==0) {
					Controller.loadBikeRoutesJSON(FilesRoutes.BIKE_ROUTES_JSON.getFileRoute());
					System.out.println("ya cargo las rutas");
				}

				//Metodo 1C //CARGA LAS ESTACIONES Q1Q2 Y Q3Q4 SOLO UNA VEZ
				if(dataStations.equals(FilesRoutes.STATIONS_Q1_Q2.getFileRoute()) && estacionesQ1Q2Cargadas) {
					Controller.loadTrips(dataTrips);
				}
				else if(dataStations.equals(FilesRoutes.STATIONS_Q3_Q4.getFileRoute()) && estacionesQ3Q4Cargadas) {
					Controller.loadTrips(dataTrips);
				}
				else {
					Controller.loadTrips(dataTrips);
					Controller.loadStations(dataStations);	
					System.out.println("ya cargo estaciones");
					if(dataStations.equals(FilesRoutes.STATIONS_Q1_Q2.getFileRoute())) {
						estacionesQ1Q2Cargadas = true;
						System.out.println("ya cargo estaciones12");
					}
					else {
						estacionesQ3Q4Cargadas = true;
						System.out.println("ya cargo estaciones34");
					}
				}

				//CARGA LAS BICLETAS 

				Controller.loadBikes();;
				System.out.println("ya cargo bicicletas");
				Controller.loadHash2BY3B();
				System.out.println("ya cargo hash2b3b");
				Controller.cargarEstructurasParteA();
				System.out.println("ya cargo parteA");

				//IMPRIME LOS MENSAJES LUEGO DE CARGAR
				System.out.println("Total de viajes cargados en el sistema: " + Controller.tripsSSize());

				System.out.println("Total estaciones cargadas en el sistema: " + Controller.getStationsSSize());

				System.out.println("Total de ciclorutas cargadas en el sistema: " + Controller.getRoutesSize());

				System.out.println("Total de bicicletas cargadas en el sistema: " + Controller.bikesSize());

				System.out.println("==================================================================");

				//Tiempo en cargar
				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");
				break;


			case 1: //Req A1

				//Capacidad
				System.out.println("Ingrese la capacidad de la estacion: (Ej: 56)");
				String capacidad = linea.next();

				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : mes/dia/a�o)");

				String fechaInicialReq1A = linea.next();


				// Datos Fecha inicial
				LocalDate localDateFin = convertirFecha(fechaInicialReq1A);

				//Metodo 1A
				DoublyLinkedList<VOTrip> viajesA1 = Controller.A1(Integer.parseInt(capacidad), localDateFin);
				if(viajesA1 == null) {
					System.out.println("No hay viajes que cumplan con esos requerimientos");
				}
				else {
					for(VOTrip v : viajesA1)
					{
						System.out.println(v.toString());
					}
					System.out.println("////////////////////////////////////////////////////");
					System.out.println("Se encontraron " + viajesA1.getSize() + " viajes. ");

				}
				break;

			case 2: //Req A2

				System.out.println("Ingrese la duracion deseada:");
				//Duracin de los viajes
				n = Integer.parseInt(linea.next());

				DoublyLinkedList<VOTrip> viajesA2 = Controller.A2(n);
				if(viajesA2 == null) {
					System.out.println("No hay viajes que duren ese tiempo");
				}
				else {
					for(VOTrip v : viajesA2)
					{
						System.out.println(v.toString());
					}
					System.out.println("////////////////////////////////////////////////////");
					System.out.println("Se encontraron " + viajesA2.getSize() + " viajes. ");
				}
				break;

			case 3: //Req A3

				//Nmero de viajes que se desan buscar
				System.out.println("Ingrese el numero de viajes a buscar: ");
				n = Integer.parseInt(linea.next());


				//Fecha
				System.out.println("Ingrese la fecha (Ej : mes/dia/a�o)");
				String fechaInicialReq3A = linea.next();

				// Datos Fecha
				LocalDate localDateInicio3A = convertirFecha(fechaInicialReq3A);

				//Metodo A3
				DoublyLinkedList<VOTrip> viajesPorBicicleta = Controller.A3(n, localDateInicio3A);
				if(viajesPorBicicleta == null) {
					System.out.println("No hay viajes en esa fecha " + localDateInicio3A);
				}
				else {
					int i = viajesPorBicicleta.getSize() - 1;
					VOTrip v;
					while(i >= 0) {
						v = viajesPorBicicleta.getElement(i);
						System.out.println(v.toString());
						System.out.println("Distancia "+ (viajesPorBicicleta.getSize()-i) + ": "+ v.getDistance());
						System.out.println("------------------------------------------------------");
						i--;
					}
					System.out.println("////////////////////////////////////////////////////");
					System.out.println("Se encontraron " + viajesPorBicicleta.getSize() + " viajes. ");
				}
				break;

			case 4: //Req B1

				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo minimo total recorrido: ");
				limiteInferior = linea.nextInt();

				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo maximo total recorrido: ");
				limiteSuperior = linea.nextInt();


				//Metodo B1
				DoublyLinkedList<VOBike> bikesB1 = Controller.B1(limiteInferior, limiteSuperior);

				for(VOBike v : bikesB1)
				{	
					System.out.println("Id: "+v.darId());
					System.out.println("Duracion total de viajes: "+v.getDuracionTotalViajes());
				}
				break;

			case 5: //Req B2

				//estacion inicial
				System.out.println("Ingrese la estacion inicial: ");

				String estacoinInicialReq1B = "";
				if(linea.hasNextLine()) {
					estacoinInicialReq1B=linea.next()+linea.nextLine();
					System.out.println(estacoinInicialReq1B);
				}

				//estacion final
				System.out.println("Ingrese la estacion final: ");
				String estacionFinalReq1B = linea.nextLine();


				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo minimo (en segundos) del recorrido: ");
				limiteInferior = linea.nextInt();

				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo maximo (en segundos) del recorrido: ");
				limiteSuperior = linea.nextInt();


				//Req B2
				DoublyLinkedList<VOTrip> viajesB2 = Controller.B2(estacoinInicialReq1B, estacionFinalReq1B, limiteInferior, limiteSuperior);

				for(VOTrip v : viajesB2)
				{
					System.out.println("ID del viaje: "+v.getTrip_id());
					System.out.println("Estacion de origen y destino: "+v.getFrom_station_name()+", "+ v.getTo_station_name());
					System.out.println("Duracion: "+v.getTripSeconds());
					System.out.println("Fecha inicio y final: "+v.getStart_time().toString()+", "+v.getEnd_time().toString());

				}
				break;


			case 6: //Req B3
				//Estacion de inicio
				System.out.println("Ingrese la estaci�n de inicio (Ej : Shedd Aquarium)");
				String estacionDeInicio="";
				if(linea.hasNextLine()) {
					estacionDeInicio=linea.next()+linea.nextLine();
					System.out.println(estacionDeInicio);
				}

				//Estacion de llegada
				System.out.println("Ingrese la estaci�n de llegada (Ej : Shedd Aquarium)");
				String estacionDeLlegada = linea.nextLine();

				int [] resultados = Controller.B3(estacionDeInicio, estacionDeLlegada);
				int horaEnteraConMayorNumeroDeViajes = resultados[0];
				int totalViajes = resultados[1];

				//TODO Imprimir con un mejor formato
				System.out.println("Hora:" + horaEnteraConMayorNumeroDeViajes);
				System.out.println("totalViajes:" + totalViajes);

				break;

			case 7: //Req C1

				System.out.println("Ingrese cuanto estaria dispuesto a pagar por punto (Ej: 3.5):");
				double valorPorPunto = Double.parseDouble(linea.next());

				System.out.println("Ingrese el n�mero de estaciones donde se quiere poner publicidad (Ej: 4):");
				int numEstacionesConPublicidad = Integer.parseInt(linea.next());

				System.out.println("Ingrese un mes entre Febrero y Noviembre (Ej: 3):");
				int mesCampanna = Integer.parseInt(linea.next());

				ResultadoCampanna res = Controller.C1(valorPorPunto, numEstacionesConPublicidad, mesCampanna);

				System.out.println("Total a pagar: " + res.costoTotal);
				System.out.println("Estaciones");

				for(VOStation t : res.estaciones)
				{
					System.out.print(t.darName());
				}
				break;

			case 8: //Req C2

				System.out.println("Ingrese el numero de divisiones en latitud (LA)");
				int LA = Integer.parseInt(linea.next());

				System.out.println("Ingrese el numero de divisiones en longitud (LO)");
				int LO = Integer.parseInt(linea.next());

				VOSector rectangulo = Controller.getCuadranteCiudad();
				System.out.println("latitud maxima: " + rectangulo.getMaxLatitud());
				System.out.println("longitud maxima: " + rectangulo.getMaxLongitud());
				System.out.println("latitud minima: " + rectangulo.getMinLatitud());
				System.out.println("longitud minima: " + rectangulo.getMinLongitud());
				break;

			case 9: //Req C3
				System.out.println("Ingrese una latitud: (74.33)");
				double latitud = Double.parseDouble(linea.next());

				System.out.println("Ingrese una longitud: (-4.33)");
				double longitud = Double.parseDouble(linea.next());

				int sector = Controller.darSector(latitud, longitud);
				System.out.println("Sector: " + sector);

				DoublyLinkedList<VOStation> estacionesCercanas = Controller.C3(latitud, longitud);

				if(estacionesCercanas.getSize()!=0){
					System.out.println("Estaciones: ");
					for(VOStation s: estacionesCercanas) {
						System.out.print("id: " + s.darId() + ", " );
						System.out.print("nombre:" + s.darName() + ", ");
						System.out.print("Distancia entre estacion y punto: "+Controller.distance(latitud, longitud, s.darLatitude(), s.darLongitude()));

					}
				}else{
					System.out.print("No hay estaciones en el sector " );
				}

				break;


			case 10: //Req C4
				System.out.println("Ingrese una latitud: (74.33)");
				double latitudR = Double.parseDouble(linea.next());

				System.out.println("Ingrese una longitud: (-4.33)");
				double longitudR = Double.parseDouble(linea.next());

				int sectorR = Controller.darSector(latitudR, longitudR);
				System.out.println("Sector: " + sectorR);

				DoublyLinkedList<VOBikeRoute> ciclorutasCercanas = Controller.C4(latitudR, longitudR);
				if(ciclorutasCercanas.getSize()!=0){

					System.out.println("Ciclorutas: ");
					for(VOBikeRoute b: ciclorutasCercanas) {

						System.out.print("Calle de referencia:" + b.getReferenceStreet() + ", ");
						GeoCoordinate[] geoLoc = b.getRoute();
						for (int i = 0; i < geoLoc.length; i++) {
							System.out.print("Localizaciones geograficas: ");
							System.out.println( "Latitud"+ b.getRoute()[i].getLatitud());
							System.out.println( "Longitud"+ b.getRoute()[i].getLongitud());
						}
					}
				}else{
					System.out.print("No hay estaciones en el sector " );
				}

					break;

				case 11: //Req C5
					System.out.println("Ingrese una latitud inicial: (74.33)");
					System.out.println("Coordenadas dentro de 1 sector: 42.020882125851536, 41.98459120239463");
					double latitudI = Double.parseDouble(linea.next());

					System.out.println("Ingrese una longitud inicial: (-4.33)");
					System.out.println("Coordenadas dentro de 1 sector: -87.78396451565798, -87.81235055856227");
					double longitudI = Double.parseDouble(linea.next());

					System.out.println("Ingrese una latitud final: (74.33)");
					System.out.println("Coordenadas dentro de 1 sector: 42.020882125851536, 41.98459120239463");
					double latitudF = Double.parseDouble(linea.next());

					System.out.println("Ingrese una longitud final: (-4.33)");
					System.out.println("Coordenadas dentro de 1 sector: -87.78396451565798, -87.81235055856227");
					double longitudF = Double.parseDouble(linea.next());

					System.out.println("Ciclorutas: ");
					DoublyLinkedList<String> ciclorutasQueSePuedenUsar = Controller.C5(latitudI, longitudI, latitudF, longitudF);
					if(ciclorutasQueSePuedenUsar == null) {
						System.out.println("No hay ciclorutas que coincidan con esas coordenadas");
					}
					else {
						for(String b: ciclorutasQueSePuedenUsar) {
							System.out.println(b);
							System.out.println("-------------------------------------------------------------");
						}
						System.out.println("////////////////////////////////////////////////////////////////");
						System.out.println("Ciclorutas encontradas: " + ciclorutasQueSePuedenUsar.getSize());
					}
					break;

				case 12: //Salir
					fin = true;
					linea.close();
					break;
				}
			}
		}

		private static void printMenu()
		{
			System.out.println("-----------------ISIS 1206 - Estructuras de Datos------======----");
			System.out.println("-------------------- Proyecto 2   - 2018-2 ----------------------");
			System.out.println("Iniciar la Fuente de Datos a Consultar :");
			System.out.println("0. Cargar datos de todos los archivos");

			System.out.println("\nParte A:\n");
			System.out.println("1. Viajes que terminaron en una estacion con cierta capacidad, en una fecha dada (1A)");
			System.out.println("2. Viajes con duraciones similares (2A)");
			System.out.println("3. Viajes con los recorridos mas largos en una fecha dada (3A)");

			System.out.println("\nParte B:\n");
			System.out.println("4. Bicicletas para mantenimiento (1B)");
			System.out.println("5. Viajes por estaciones de salida y llegada en un rango de tiempo (2B)");
			System.out.println("6. Hora pico de viajes por estaciones de salida y llegada (3B)");



			System.out.println("\nParte C:\n");
			System.out.println("7. Campa�a de publicidad (2C)");
			System.out.println("8. Sectorizacion (3C)");
			System.out.println("9. Busqueda georreferenciada (latitud y longitud) de estaciones");
			System.out.println("10. Busqueda georreferenciada (latitud y longitud) de ciclorutas");
			System.out.println("11. Busqueda de ciclorutas para hacer viaje");
			System.out.println("12. Salir");
			System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

		}

		/**
		 * Convertir fecha a un objeto LocalDate
		 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
		 * @return objeto LD con fecha
		 */
		private static LocalDate convertirFecha(String fecha)
		{
			String[] datosFecha = fecha.split("/");


			int agno = Integer.parseInt(datosFecha[2]);
			int mes = Integer.parseInt(datosFecha[0]);
			int dia = Integer.parseInt(datosFecha[1]);

			return LocalDate.of(agno, mes, dia);
		}

		/**
		 * Convertir fecha y hora a un objeto LocalDateTime
		 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
		 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
		 * @return objeto LDT con fecha y hora integrados
		 */
		private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
		{
			String[] datosFecha = fecha.split("/");
			String[] datosHora = hora.split(":");

			int agno = Integer.parseInt(datosFecha[2]);
			int mes = Integer.parseInt(datosFecha[1]);
			int dia = Integer.parseInt(datosFecha[0]);
			int horas = Integer.parseInt(datosHora[0]);
			int minutos = Integer.parseInt(datosHora[1]);
			int segundos = Integer.parseInt(datosHora[2]);

			return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
		}

		private static void printMenuCargar()
		{
			System.out.println("-- Que fuente de datos desea agregar a los datos del sistema (carga incremental)?");
			System.out.println("-- 1. 2017-Q1");
			System.out.println("-- 2. 2017-Q2");
			System.out.println("-- 3. 2017-Q3");
			System.out.println("-- 4. 2017-Q4");
			System.out.println("-- 5. Reiniciar datos del sistema");		
			System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
		}


	}