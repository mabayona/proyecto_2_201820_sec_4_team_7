package utils;

import java.util.Comparator;

import model.vo.VOBike;

public class BikesComparatorByDuration implements Comparator<VOBike>{

	@Override
	public int compare(VOBike o1, VOBike o2) {
		double comparacion = o1.getDuracionTotalViajes() - o2.getDuracionTotalViajes();
		if(comparacion > 0)
			return 1;
		else if(comparacion < 0)
			return -1;
		else
			return 0;
	}

}
