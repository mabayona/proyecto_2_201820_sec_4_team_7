package api;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {
	
	//==========================================================
	//METODOS DE CARGA
	//==========================================================
	
	/**
	 * Metodo paa cargar los datos de los viajes
	 * @param tripsFile - path to the file 
	 */
	void loadTrips(String tripsFile);

	/**
	 * Metodo paa cargar los datos de las estaciones
	 * @param tripsFile - path to the file 
	 */
	void loadStations(String stationsFile);

	/**
	 * Method to load the Bike_Routes 
	 * @param BikeRoutesFile - path to the file 
	 * COMPLEJIDAD APROX: n
	 */
	void loadBikeRoutesJSON(String bikeRoutesFile);

	
	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE A 
	//==========================================================
	
	
	
	
	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE B 
	//==========================================================



	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE C 
	//==========================================================



}
