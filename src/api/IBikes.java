package api;

import java.time.LocalDateTime;
import java.util.Date;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Stack;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;

public interface IBikes {


	DoublyLinkedList<VOBike> getBikesListFromTrips(VOTrip[] tripsList, Stack<VOStation> stationsS );
	
	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE A 
	//==========================================================
	
	
	
	
	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE B 
	//==========================================================



	//==========================================================
	//METODOS DE LOS REQUERIMIENTOS PARTE C 
	//==========================================================


}
